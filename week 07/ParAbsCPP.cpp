#include<iostream>
using namespace std;
class Bird
{
    public:
      virtual void fly()
    {
        cout<<"i can fly in the sky"<<endl;
    }
    
    virtual void eat()=0;
};
class parrot : public Bird
{
    public:
    
    void eat()
    {
        cout<<"i eat guava"<<endl;
    }
};
int main()
{
    parrot obj;
    obj.fly();
    obj.eat();
    return 0;
}
