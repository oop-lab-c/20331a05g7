import java.util.*;
class Main
{
    public static void main(String[] args)
    {
        int a,b;
        Scanner input=new Scanner(System.in);
        System.out.println("Enter two numbers : ");
        a=input.nextInt();
        b=input.nextInt();
        try
        {
            System.out.println("Quotient :" + a/b);
        }
        catch(ArithmeticException e)
        {
            System.out.println("Denominator should not be zero");
        }
        finally
        {
            System.out.println("end");
        }
    }
}
