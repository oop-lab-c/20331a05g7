#include<iostream>
using namespace std;
class A
{
    public:
    int a;
    A()
    {
        cout<<"constructor of A is called"<<endl;
    }
};
class B:virtual public A
    public:
    int b;
    B()
    {
        cout<<"constructor of B is called"<<endl;
    }
};
class C:virtual public A
    public:
    int c;
    C()
    {
        cout<<"constructor of C is called"<<endl;
    }
};
class D:public B,public C
{
    public:
    int d;
    D()
    {
        cout<<"constructor of D is called"<<endl;
    }
};
void display(int a,int b,int c,int d)
{
    cout<<"Displaying the values :"<<endl;
    cout<<a<<" "<<b<<" "<<c<<" "<<d<<endl;
}
int main()
{
    int i1,i2,i3,i4;
    D obj;
    cout<<"Enter 4 integers :"<<endl;
    cin>>i1>>i2>>i3>>i4;
    display(i1,i2,i3,i4);
    return 0;
}